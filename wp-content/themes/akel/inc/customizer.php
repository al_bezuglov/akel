<?php

function akel_custom_customize_register( $wp_customize ) {

   //social vk link
	$wp_customize->add_setting('social_vk', array(
		'default' => '',
		'sanitize_callback' => 'eightstore_lite_sanitize_text',
		));

	$wp_customize->add_control('social_vk',array(
		'type' => 'text',
		'label' => __('Vkontakte','eightstore-lite'),
		'section' => 'social_setting',
		'setting' => 'social_vk'
		));

}

//add_action( 'customize_register', 'akel_custom_customize_register' );